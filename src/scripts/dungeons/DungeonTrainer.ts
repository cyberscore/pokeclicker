///<reference path="../trainers/Trainer.ts"/>
import Trainer from "../trainers/Trainer.ts";

export default class DungeonTrainer extends Trainer {

    constructor(
        trainerClass: string,
        team: GymPokemon[],
        public options?: EnemyOptions,
        name?: string,
        subTrainerClass?: string) {
        super(trainerClass, team, name, subTrainerClass);
    }

}
