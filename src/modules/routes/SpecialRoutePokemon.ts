import { PokemonNameType } from '../pokemons/PokemonNameType.ts';
import Requirement from '../requirements/Requirement.ts';

export default class SpecialRoutePokemon {
    constructor(
        public pokemon: PokemonNameType[],
        public req: Requirement,
    ) {}

    isAvailable(): boolean {
        return this.req.isCompleted();
    }
}
