import { Pokeball as PokeballType, Currency } from '../GameConstants.ts';
import Item from './Item.ts';
import { ShopOptions } from './types.ts';

export default class PokeballItem extends Item {
    type: PokeballType;

    constructor(type: PokeballType, basePrice: number, currency: Currency = Currency.money, options?: ShopOptions, displayName?: string) {
        super(PokeballType[type], basePrice, currency, options, displayName, 'A ball for catching Pokémon.', 'pokeball');
        this.type = type;
    }

    gain(amt: number) {
        App.game.pokeballs.gainPokeballs(this.type, amt);
    }

}
