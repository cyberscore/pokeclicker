import CaughtStatus from '../enums/CaughtStatus.ts';
import Item from './Item.ts';

export default abstract class CaughtIndicatingItem extends Item {
    abstract getCaughtStatus(): CaughtStatus;
}
