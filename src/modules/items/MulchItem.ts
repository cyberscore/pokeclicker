import MulchType from '../enums/MulchType.ts';
import { Currency } from '../GameConstants.ts';
import GameHelper from '../GameHelper.ts';
import Item from './Item.ts';
import { MultiplierDecreaser } from './types.ts';

export default class MulchItem extends Item {
    type: MulchType;

    constructor(type: MulchType, basePrice: number, displayName: string, description: string) {
        super(MulchType[type], basePrice, Currency.farmPoint, { multiplierDecreaser: MultiplierDecreaser.Berry }, displayName, description, 'farm');
        this.type = type;
    }

    gain(amt: number) {
        GameHelper.incrementObservable(App.game.farming.mulchList[this.type], amt);
    }
}
