import { PokeBlockColor, Currency } from '../GameConstants.ts';
import Item from './Item.ts';

export default class PokeBlock extends Item {
    type: PokeBlockColor;

    constructor(color: PokeBlockColor, basePrice: number, currency: Currency = Currency.money) {
        super(`PokeBlock_${PokeBlockColor[color]}`, basePrice, currency);
        this.type = color;
    }
    
    get description(): string {
        return this._description || 'Unobtainable item for future uses';
    }
}
