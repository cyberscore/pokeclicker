import { Currency } from '../GameConstants.ts';
import Amount from './Amount.ts';

export default abstract class AmountFactory {
    static createArray(amounts: number[], currency: Currency): Amount[] {
        return amounts.map((amt) => new Amount(amt, currency));
    }
}
