export default class ko {
  static observable(value) {
    const f = function(newValue = null) {
      if (newValue === null) {
        return f.value;
      } else {
        f.value = newValue;
      }
    };
    f.extend = function() { return this; };
    f.subscribe = function() {};
    f.peek = function() { return this.value; };
    f.value = value;
    f.__observable__ = true;
    return f;
  }
  static observableArray(value) {
    const f = ko.observable(value);

    f.push = function(v) { this.value.push(v); };

    return f;
  }
  static pureComputed(fn) {
    const f = function() {
      return fn();
    };
    f.extend = function() { return this; };
    f.subscribe = function() {};
    f.__observable__ = true;
    return f;
  }
  static computed(fn) {
    const f = function() {
      return fn();
    };
    f.extend = function() { return this; };
    f.subscribe = function() {};
    f.__observable__ = true;
    return f;
  }
  static isObservable(obj) {
    return obj && obj.__observable__;
  }
}
