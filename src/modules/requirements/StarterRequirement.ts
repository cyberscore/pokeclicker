import {
    AchievementOption, camelCaseToString, Region, RegionalStarters, Starter,
} from '../GameConstants.ts';
import { pokemonList } from '../pokemons/PokemonList.ts';
import Requirement from './Requirement.ts';

export default class StarterRequirement extends Requirement {
    constructor(public region: Region, private starter: Starter) {
        super(starter, AchievementOption.equal);
    }

    public getProgress() {
        const starter = player.regionStarters[this.region]();
        return starter === Starter.None ? Starter.Grass : starter;
    }

    public hint(): string {
        const starter = pokemonList.find((p) => p.id === RegionalStarters[this.region][this.starter]).name;
        return `Requires ${starter} to be chosen as your ${camelCaseToString(Region[this.region])} starter Pokémon.`;
    }
}
