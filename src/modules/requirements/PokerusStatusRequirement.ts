import * as GameConstants from '../GameConstants.ts';
import AchievementRequirement from './AchievementRequirement.ts';

export default class PokerusStatusRequirement extends AchievementRequirement {
    constructor(pokemonRequired: number, public statusRequired: GameConstants.Pokerus) {
        super(pokemonRequired, GameConstants.AchievementOption.more, GameConstants.AchievementType.Pokerus);
    }

    public getProgress() {
        return Math.min(App.game.party.caughtPokemon.filter((p) => p.pokerus >= this.statusRequired).length, this.requiredValue);
    }

    public hint(): string {
        return `${this.requiredValue} Pokémon needs to be infected.`;
    }

    public toString(): string {
        return `${super.toString()} ${this.statusRequired}`;
    }
}
