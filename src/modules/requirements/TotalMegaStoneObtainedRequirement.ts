import * as GameConstants from '../GameConstants.ts';
import { ItemList } from '../items/ItemList.ts';
import MegaStoneItem from '../items/MegaStoneItem.ts';
import AchievementRequirement from './AchievementRequirement.ts';

export default class TotalMegaStoneObtainedRequirement extends AchievementRequirement {
    constructor(value: number) {
        super(value, GameConstants.AchievementOption.more, GameConstants.AchievementType['Mega Stone']);
    }

    public getProgress() {
        return Math.min(Object.values(ItemList).filter((i) => i instanceof MegaStoneItem && player.itemList[i.name]()).length, this.requiredValue);
    }

    public hint(): string {
        return `${this.requiredValue} Mega Stones need to be obtained.`;
    }
}
