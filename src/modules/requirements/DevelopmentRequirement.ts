import * as GameConstants from '../GameConstants.ts';
import AchievementRequirement from './AchievementRequirement.ts';
import GameHelper from '../GameHelper.ts';
import MultiRequirement from './MultiRequirement.ts';
import Requirement from './Requirement.ts';

export default class DevelopmentRequirement extends AchievementRequirement {
    private static default: DevelopmentRequirement;
    development = false;

    constructor(public requirement : Requirement | MultiRequirement = null) {
        super(1, GameConstants.AchievementOption.more);

        if (!requirement) {
            if (DevelopmentRequirement.default) {
                return DevelopmentRequirement.default;
            }
            DevelopmentRequirement.default = this;
        }

        this.development = GameHelper.isDevelopmentBuild();
    }

    // eslint-disable-next-line class-methods-use-this
    public getProgress() {
        return +(this.development && (this.requirement?.isCompleted() ?? true));
    }

    // eslint-disable-next-line class-methods-use-this
    public hint(): string {
        return !this.development ? 'This is probably still under development.' : this.requirement?.hint() ?? 'This is probably still under development.';
    }
}
