import ko from "../../modules/ko.ts";
import { AchievementOption } from '../GameConstants.ts';
import Requirement from './Requirement.ts';

export default class CustomRequirement<T> extends Requirement {
    constructor(
        private focus: Observable<T> | Computed<T>,
        private required: T,
        private hintText: string,
        option = AchievementOption.more,
    ) {
        super(1, option);
    }

    public getProgress() {
        return Number(this.focus() === this.required);
    }

    public hint(): string {
        return this.hintText;
    }
}
