import { AchievementOption, camelCaseToString } from '../GameConstants.ts';
import Requirement from './Requirement.ts';
import MoonCyclePhase from '../moonCycle/MoonCyclePhase.ts';
import MoonCycle from '../moonCycle/MoonCycle.ts';

export default class MoonCyclePhaseRequirement extends Requirement {
    constructor(public moonCyclePhases: MoonCyclePhase[], option = AchievementOption.more) {
        super(1, option);
    }

    public getProgress(): number {
        return Number(this.moonCyclePhases.includes(MoonCycle.currentMoonCyclePhase()));
    }

    public hint(): string {
        return `The moon phase must be ${camelCaseToString(this.moonCyclePhases.map((moonCyclePhase) => MoonCyclePhase[moonCyclePhase]).join(' or '))}`;
    }
}
