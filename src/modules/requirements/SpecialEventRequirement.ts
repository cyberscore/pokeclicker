import { AchievementOption } from '../GameConstants.ts';
import { SpecialEventTitleType } from '../specialEvents/SpecialEventTitleType.ts';

import Requirement from './Requirement.ts';

export default class SpecialEventRequirement extends Requirement {
    constructor(private specialEventName: SpecialEventTitleType) {
        super(1, AchievementOption.equal);
    }

    public getProgress(): number {
        return +(App.game.specialEvents.getEvent(this.specialEventName).isActive());
    }

    public hint(): string {
        return `${this.specialEventName} must be started.`;
    }
}
