import { AchievementOption, humanifyString } from '../GameConstants.ts';
import GameHelper from '../GameHelper.ts';
import { ItemNameType } from '../items/ItemNameType.ts';
import { PokemonNameType } from '../pokemons/PokemonNameType.ts';
import Requirement from './Requirement.ts';

export default class HoldingItemRequirement extends Requirement {
    constructor(public pokemon: PokemonNameType, public itemName: ItemNameType, option = AchievementOption.more) {
        super(1, option);
    }

    public getProgress() {
        const heldItem = App.game.party.getPokemonByName(this.pokemon)?.heldItem();
        return heldItem?.name === this.itemName ? 1 : 0;
    }

    public hint(): string {
        return `Your pokemon must ${
            this.option == AchievementOption.less ? 'not' : ''
        } be holding ${
            GameHelper.anOrA(this.itemName)
        } ${
            humanifyString(this.itemName)
        }.`;
    }
}
