import ItemType from '../enums/ItemType.ts';
import Requirement from '../requirements/Requirement.ts';

export default interface BagItem {
    type: ItemType,
    id: string | number,
    requirement?: Requirement,
}
