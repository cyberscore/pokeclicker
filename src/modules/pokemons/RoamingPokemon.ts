import MultiRequirement from '../requirements/MultiRequirement.ts';
import OneFromManyRequirement from '../requirements/OneFromManyRequirement.ts';
import Requirement from '../requirements/Requirement.ts';
import { PokemonListData, pokemonMap } from './PokemonList.ts';
import { PokemonNameType } from './PokemonNameType.ts';

export default class RoamingPokemon {
    public pokemon: PokemonListData;

    constructor(
        public pokemonName: PokemonNameType,
        public unlockRequirement?: Requirement | MultiRequirement | OneFromManyRequirement,
    ) {
        this.pokemon = pokemonMap[pokemonName];
    }

    public isRoaming() {
        return this.unlockRequirement ? this.unlockRequirement.isCompleted() : true;
    }
}
