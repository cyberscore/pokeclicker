import { SubRegions } from '../GameConstants.ts';

export default class RoamingGroup {
    public subRegions: SubRegions[];
    public name: string;

    constructor(name: string, subRegions: SubRegions[]) {
        this.name = name;
        this.subRegions = subRegions;
    }
}
