import BadgeCase from "./DataStore/BadgeCase.ts";
import Challenges from "./challenges/Challenges.ts";
import Settings from './settings/index.ts';
import Multiplier from './multiplier/Multiplier.ts';
import Statistics from './DataStore/StatisticStore/index.ts';
import Party from '../scripts/party/Party.ts';

const app = {
  game: {
    badgeCase: new BadgeCase(),
    multiplier: new Multiplier(),
    challenges: new Challenges(),
    statistics: new Statistics(),
    party: new Party(),
  },
};

export default app;

export function loadSaveIntoApp(save) {
  app.game.badgeCase.fromJSON(save.save.badgeCase);
  app.game.challenges.fromJSON(save.save.challenges);
  app.game.statistics.fromJSON(save.save.statistics);
  app.game.party.fromJSON(save.save.party);

  Settings.fromJSON(save.settings);
}
